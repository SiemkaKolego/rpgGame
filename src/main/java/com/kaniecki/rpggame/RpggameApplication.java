package com.kaniecki.rpggame;

import com.kaniecki.rpggame.battleField.service.IBattleField;
import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import com.kaniecki.rpggame.common.unit.Type;
import com.kaniecki.rpggame.unit.dto.SpecialAbilityDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import com.kaniecki.rpggame.user.dto.UserDto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RpggameApplication {

    public static void main(String[] args) {
        SpringApplication.run(RpggameApplication.class, args);

        UserDto player = createPlayer();
        UserDto defender = createDefender();

        ApplicationContext context = new AnnotationConfigApplicationContext(RpggameApplication.class);
        IBattleField battleField = context.getBean(IBattleField.class);
        battleField.performBattle(player, defender);
    }

    private static UserDto createPlayer() {
        UserDto player = new UserDto();
        player.setName("PLAYER");
        player.setHealth(50);
        player.setUnitsInDeck(generateUnitsInDeck());
        player.setAllUnitsAvailable(generateUnitsInDeck());
        player.setGold(0);

        return player;
    }

    private static UserDto createDefender() {
        UserDto defender = new UserDto();
        defender.setName("DEFENDER");
        defender.setHealth(50);
        defender.setUnitsInDeck(generateUnitsInDeck());

        return defender;
    }

    private static Set<UnitDto> generateUnitsInDeck() {
        Set<UnitDto> unitsInDeck = new HashSet<>();

        unitsInDeck.add(generateUnit(Type.KNIGHT, null));
        unitsInDeck.add(generateUnit(Type.KNIGHT, generateSpecialAbility(SpecialAbilityType.BLOCK)));
        unitsInDeck.add(generateUnit(Type.WARRIOR, null));
        unitsInDeck.add(generateUnit(Type.WARRIOR, generateSpecialAbility(SpecialAbilityType.VAMPIRISM)));
        unitsInDeck.add(generateUnit(Type.MAGE, null));
        unitsInDeck.add(generateUnit(Type.MAGE, generateSpecialAbility(SpecialAbilityType.HEAL)));
        unitsInDeck.add(generateUnit(Type.ARCHER, null));

        return unitsInDeck;
    }

    private static UnitDto generateUnit(Type type, List<SpecialAbilityDto> specialAbilities) {
        UnitDto unit = new UnitDto();

        unit.setName(type.toString());
        unit.setType(type);
        unit.setHealth(10);
        unit.setAttack(4);
        unit.setLevel(2);
        unit.setSpecialAbilities(specialAbilities);
        unit.setCurrentExperience(0);

        return unit;
    }

    private static List<SpecialAbilityDto> generateSpecialAbility(SpecialAbilityType type) {
        List<SpecialAbilityDto> specialAbilities = new ArrayList<>();

        specialAbilities.add(new SpecialAbilityDto(type, 1));

        return specialAbilities;
    }
}
