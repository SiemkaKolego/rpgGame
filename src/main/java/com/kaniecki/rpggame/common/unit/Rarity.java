package com.kaniecki.rpggame.common.unit;

public enum Rarity {
    COMMON, RARE, SUPER_RARE, SUPER_SUPER_RARE
}
