package com.kaniecki.rpggame.common.unit;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

public class UnitConstantValues {

    public static final Set<SpecialAbilityType> ALL_SPECIAL_ABILITY_TYPES = Collections.unmodifiableSet(EnumSet.allOf(SpecialAbilityType.class));

    public static final int LEVEL_FOR_NEW_SPECIAL_ABILITY = 5;
    public static final int LEVEL_FOR_FIRST_INCREMENT_SPECIAL_ABILITY = 4;
    public static final int FIRST_SPECIAL_ABILITY_INITIAL_VALUE = 2;
    public static final int SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE = 2;
    public static final int NEXT_SPECIAL_ABILITY_INITIAL_VALUE = 1;
    public static final int INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY = 2;
    public static final int INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY = 1;

    public static final int HEALTH_INCREMENT_VALUE_FOR_KNIGHT = 4;
    public static final int HEALTH_INCREMENT_VALUE_FOR_WARRIOR = 3;
    public static final int HEALTH_INCREMENT_VALUE_FOR_ARCHER = 2;
    public static final int HEALTH_INCREMENT_VALUE_FOR_MAGE = 1;

    public static final int ATTACK_INCREMENT_VALUE_FOR_KNIGHT = 1;
    public static final int ATTACK_INCREMENT_VALUE_FOR_WARRIOR  = 2;
    public static final int ATTACK_INCREMENT_VALUE_FOR_ARCHER = 3;
    public static final int ATTACK_INCREMENT_VALUE_FOR_MAGE = 4;

    public static final int UNITS_DECK_MAX_CAPACITY = 7;
    public static final int UNIT_DECK_FOR_BATTLE_TIME = 4;
}
