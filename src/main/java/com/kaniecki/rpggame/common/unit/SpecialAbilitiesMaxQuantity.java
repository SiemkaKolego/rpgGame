package com.kaniecki.rpggame.common.unit;

public enum SpecialAbilitiesMaxQuantity {
    RARE_TYPE_MAX(1), SUPER_RARE_MAX(2), SUPER_SUPER_RARE_MAX(3);

    public final int value;

    SpecialAbilitiesMaxQuantity(int value) {
        this.value = value;
    }
}
