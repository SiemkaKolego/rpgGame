package com.kaniecki.rpggame.common.unit;

public enum SpecialAbilityType {
    BLOCK("Block"), SPIKES("Spikes"), HEAL("Heal"), PIERCE("Pierce"), VAMPIRISM("Vampirism"), INCREASE_ATTACK("Increase attack");

    private final String name;

    SpecialAbilityType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
