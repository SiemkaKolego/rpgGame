package com.kaniecki.rpggame.common.unit;

public enum Type {
    KNIGHT, MAGE, ARCHER, WARRIOR
}
