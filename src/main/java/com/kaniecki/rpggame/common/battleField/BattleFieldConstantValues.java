package com.kaniecki.rpggame.common.battleField;

public class BattleFieldConstantValues {

    public static final int MAGE_OR_ARCHER_FORBIDDEN_COLUMN_PLAYER_SIDE = 3;
    public static final int KNIGHT_OR_WARRIOR_FORBIDDEN_COLUMN_PLAYER_SIDE = 1;
    public static final int MAGE_OR_ARCHER_FORBIDDEN_COLUMN_ENEMY_SIDE = 1;
    public static final int KNIGHT_OR_WARRIOR_FORBIDDEN_COLUMN_ENEMY_SIDE = 3;

    public static final int FIELD_CAPACITY = 12;
}
