package com.kaniecki.rpggame.common.box;

public class BoxConstantValues {

    public static final int USER_BOXES_MAX_CAPACITY = 5;

    public static final int UNIT_EXPERIENCE_MIN = 10;
    public static final int UNIT_EXPERIENCE_MAX = 30;

    public static final int GOLD_MIN = 20;
    public static final int GOLD_MAX = 50;
}
