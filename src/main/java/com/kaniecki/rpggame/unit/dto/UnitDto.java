package com.kaniecki.rpggame.unit.dto;

import com.kaniecki.rpggame.common.unit.Rarity;
import com.kaniecki.rpggame.common.unit.Type;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UnitDto {

    private String name;
    private int level;
    private int currentExperience;
    private int nextLevelExperience;
    private int health;
    private int attack;
    private int specialAbilitiesMaxQuantity;
    private List<SpecialAbilityDto> specialAbilities;
    private Type type;
    private Rarity rarity;
    //TODO add unit strength and increase with each level

    @Override
    public String toString() {
        return this.name + " -> " + "H:" + this.health + " A:" + this.attack;
    }

    public UnitDto() {
    }

    public UnitDto(UnitDto other) {
        this.name = other.getName();
        this.level = other.getLevel();
        this.health = other.getHealth();
        this.attack = other.getAttack();
        this.specialAbilities = createSpecialAbilitiesCopy(other.getSpecialAbilities());
        this.type = other.getType();
        this.rarity = other.getRarity();
    }

    private List<SpecialAbilityDto> createSpecialAbilitiesCopy(List<SpecialAbilityDto> specialAbilities) {
        List<SpecialAbilityDto> specialAbilitiesCopy = new ArrayList<>();

        if (specialAbilities != null && !specialAbilities.isEmpty()) {
            for (SpecialAbilityDto specialAbility : specialAbilities) {
                specialAbilitiesCopy.add(new SpecialAbilityDto(specialAbility));
            }
        }

        return specialAbilitiesCopy;
    }
}
