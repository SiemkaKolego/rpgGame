package com.kaniecki.rpggame.unit.dto;

import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import lombok.Data;

@Data
public class SpecialAbilityDto {

    private String name;
    private SpecialAbilityType type;
    private int value;

    public SpecialAbilityDto() {

    }

    public SpecialAbilityDto(SpecialAbilityType type, int value) {
        this.type = type;
        this.value = value;
        this.name = type.toString();
    }

    public SpecialAbilityDto(SpecialAbilityDto other) {
        this(other.getType(), other.getValue());
    }
}
