package com.kaniecki.rpggame.unit.mapper;

import com.kaniecki.rpggame.unit.dao.UnitEntity;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UnitMapper {

    UnitEntity unitDtoToUnitEntity(UnitDto source);
    UnitDto unitEntityToUnitDto(UnitEntity source);
}
