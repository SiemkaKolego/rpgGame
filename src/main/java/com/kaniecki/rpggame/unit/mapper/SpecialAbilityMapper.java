package com.kaniecki.rpggame.unit.mapper;

import com.kaniecki.rpggame.unit.dao.SpecialAbility;
import com.kaniecki.rpggame.unit.dto.SpecialAbilityDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SpecialAbilityMapper {

    SpecialAbility specialAbilityDtoToSpecialAbility(SpecialAbilityDto source);
    SpecialAbilityDto specialAbilityToSpecialAbilityDto(SpecialAbility source);

    //TODO - add mapper tests
}
