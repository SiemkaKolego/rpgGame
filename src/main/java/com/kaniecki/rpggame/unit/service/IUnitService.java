package com.kaniecki.rpggame.unit.service;

import com.kaniecki.rpggame.unit.dto.UnitDto;

public interface IUnitService {

    void levelUp(UnitDto unit);
}
