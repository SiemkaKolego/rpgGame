package com.kaniecki.rpggame.unit.service;

import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import com.kaniecki.rpggame.unit.dto.SpecialAbilityDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.kaniecki.rpggame.common.unit.UnitConstantValues.ALL_SPECIAL_ABILITY_TYPES;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.FIRST_SPECIAL_ABILITY_INITIAL_VALUE;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.LEVEL_FOR_FIRST_INCREMENT_SPECIAL_ABILITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.LEVEL_FOR_NEW_SPECIAL_ABILITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.NEXT_SPECIAL_ABILITY_INITIAL_VALUE;

@Service
public class UnitService implements IUnitService {

    @Override
    public void levelUp(UnitDto unit) {
        if (canLevelUp(unit)) {
            incrementLevel(unit);
            setExperience(unit);
            incrementHealthAndAttack(unit);
            incrementOrAddSpecialAbilities(unit);
        } else {
            System.out.println("Not enough experience!");
        }
    }

    private void incrementLevel(UnitDto unit) {
        int currentLevel = unit.getLevel();
        unit.setLevel(Math.incrementExact(currentLevel));
    }

    private void setExperience(UnitDto unit) {
        int leftExperience = Math.subtractExact(unit.getCurrentExperience(), unit.getNextLevelExperience());
        unit.setCurrentExperience(leftExperience);
        unit.setNextLevelExperience((int) (10 + Math.pow(unit.getLevel(), 3)));
    }

    private void incrementHealthAndAttack(UnitDto unit) {
        switch (unit.getType()) {
            case KNIGHT:
                setAttack(unit, 1);
                setHealth(unit, 4);
                break;
            case WARRIOR:
                setAttack(unit, 2);
                setHealth(unit, 3);
                break;
            case ARCHER:
                setAttack(unit, 3);
                setHealth(unit, 2);
                break;
            case MAGE:
                setAttack(unit, 4);
                setHealth(unit, 1);
                break;
        }
    }

    private void incrementOrAddSpecialAbilities(UnitDto unit) {
        switch (unit.getRarity()) {
            case RARE:
                if ((unit.getSpecialAbilities() == null || unit.getSpecialAbilities().size() < unit.getSpecialAbilitiesMaxQuantity()) &&
                        unit.getLevel() == LEVEL_FOR_NEW_SPECIAL_ABILITY) {
                    addFirstSpecialAbility(unit);
                    break;
                }

                if (unit.getSpecialAbilities().size() == 1 && unit.getLevel() % 2 == 0) {
                    incrementSpecialAbility(unit.getSpecialAbilities().get(0), INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY);
                }
                break;
            case SUPER_RARE:
                if (unit.getSpecialAbilities().size() < unit.getSpecialAbilitiesMaxQuantity() && unit.getLevel() == LEVEL_FOR_NEW_SPECIAL_ABILITY) {
                    addNextSpecialAbility(unit);
                }
                if (shouldIncrementEvenSpecialAbility(unit)) {
                    incrementSpecialAbility(unit.getSpecialAbilities().get(0), INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY);
                }
                if (shouldIncrementOddSpecialAbility(unit)) {
                    incrementSpecialAbility(unit.getSpecialAbilities().get(1), INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY);
                }
                break;
            case SUPER_SUPER_RARE:
                incrementSpecialAbility(unit.getSpecialAbilities().get(0), INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY);

                if (shouldIncrementEvenSpecialAbility(unit)) {
                    incrementSpecialAbility(unit.getSpecialAbilities().get(1), INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY);
                }
                if (shouldIncrementOddSpecialAbility(unit)) {
                    incrementSpecialAbility(unit.getSpecialAbilities().get(2), INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY);
                }
                break;
        }
    }

    private boolean shouldIncrementEvenSpecialAbility(UnitDto unit) {
        return unit.getLevel() >= LEVEL_FOR_FIRST_INCREMENT_SPECIAL_ABILITY && unit.getLevel() % 2 == 0;
    }

    private boolean shouldIncrementOddSpecialAbility(UnitDto unit) {
        return unit.getLevel() >= LEVEL_FOR_FIRST_INCREMENT_SPECIAL_ABILITY && unit.getLevel() % 3 == 0;
    }

    private void addFirstSpecialAbility(UnitDto unit) {
        switch (unit.getType()) {
            case WARRIOR:
                unit.setSpecialAbilities(createFirstSpecialAbility(unit, SpecialAbilityType.VAMPIRISM));
                break;
            case KNIGHT:
                unit.setSpecialAbilities(createFirstSpecialAbility(unit, SpecialAbilityType.BLOCK));
                break;
            case MAGE:
                unit.setSpecialAbilities(createFirstSpecialAbility(unit, SpecialAbilityType.INCREASE_ATTACK));
                break;
            case ARCHER:
                unit.setSpecialAbilities(createFirstSpecialAbility(unit, SpecialAbilityType.PIERCE));
                break;
        }
    }

    private List<SpecialAbilityDto> createFirstSpecialAbility(UnitDto unit, SpecialAbilityType type) {
        List<SpecialAbilityDto> specialAbilities = new ArrayList<>();

        SpecialAbilityDto specialAbility = new SpecialAbilityDto(type, FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        specialAbilities.add(specialAbility);
        unit.setSpecialAbilities(specialAbilities);

        return specialAbilities;
    }

    private void addNextSpecialAbility(UnitDto unit) {
        switch (unit.getType()) {
            case WARRIOR:
                chooseAndAddSpecialAbility(unit, SpecialAbilityType.VAMPIRISM);
                break;
            case KNIGHT:
                chooseAndAddSpecialAbility(unit, SpecialAbilityType.BLOCK);
                break;
            case MAGE:
                chooseAndAddSpecialAbility(unit, SpecialAbilityType.INCREASE_ATTACK);
                break;
            case ARCHER:
                chooseAndAddSpecialAbility(unit, SpecialAbilityType.PIERCE);
                break;
        }
    }

    private void chooseAndAddSpecialAbility(UnitDto unit, SpecialAbilityType abilityToExclude) {
        List<SpecialAbilityType> availableAbilityTypes = getAvailableAbilityTypes(abilityToExclude);
        pickRandomNewSpecialAbilityType(availableAbilityTypes, unit);
    }

    private void pickRandomNewSpecialAbilityType(List<SpecialAbilityType> availableAbilities, UnitDto unit) {
        Random random = new Random();
        SpecialAbilityType newSpecialAbilityType = availableAbilities.get(random.nextInt(availableAbilities.size()));
        unit.getSpecialAbilities().add(new SpecialAbilityDto(newSpecialAbilityType, NEXT_SPECIAL_ABILITY_INITIAL_VALUE));
    }

    private List<SpecialAbilityType> getAvailableAbilityTypes(SpecialAbilityType specialAbilityTypeToExclude) {
        List<SpecialAbilityType> availableAbilityTypes = new ArrayList<>();

        for (SpecialAbilityType ability : ALL_SPECIAL_ABILITY_TYPES) {
            if (!specialAbilityTypeToExclude.equals(ability)) {
                availableAbilityTypes.add(ability);
            }
        }

        return availableAbilityTypes;
    }

    private void incrementSpecialAbility(SpecialAbilityDto specialAbility, int incrementValue) {
        int currentValue = specialAbility.getValue();
        specialAbility.setValue(currentValue + incrementValue);
    }

    private void setAttack(UnitDto unit, int incrementValue) {
        int currentAttack = unit.getAttack();
        unit.setAttack(currentAttack + incrementValue);
    }

    private void setHealth(UnitDto unit, int incrementValue) {
        int currentHealth = unit.getHealth();
        unit.setHealth(currentHealth + incrementValue);
    }

    private boolean canLevelUp(UnitDto unit) {
        return unit.getCurrentExperience() >= unit.getNextLevelExperience();
    }
}
