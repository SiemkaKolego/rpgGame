package com.kaniecki.rpggame.unit.dao;

import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import lombok.Data;

@Data
public class SpecialAbility {

    private String name;
    private SpecialAbilityType type;
    private int value;
}
