package com.kaniecki.rpggame.unit.dao;

import com.kaniecki.rpggame.common.unit.Rarity;
import com.kaniecki.rpggame.common.unit.Type;
import lombok.Data;

import java.util.List;

@Data
public class UnitEntity {

    private String name;
    private int level;
    private int currentExperience;
    private int nextLevelExperience;
    private int health;
    private int attack;
    private int specialAbilitiesMaxQuantity;
    private List<SpecialAbility> specialAbilities;
    private Type type;
    private Rarity rarity;
}
