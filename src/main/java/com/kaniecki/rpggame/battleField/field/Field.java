package com.kaniecki.rpggame.battleField.field;

import com.kaniecki.rpggame.unit.dto.UnitDto;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.kaniecki.rpggame.common.battleField.BattleFieldConstantValues.FIELD_CAPACITY;
import static com.kaniecki.rpggame.common.battleField.BattleFieldConstantValues.KNIGHT_OR_WARRIOR_FORBIDDEN_COLUMN_PLAYER_SIDE;
import static com.kaniecki.rpggame.common.battleField.BattleFieldConstantValues.MAGE_OR_ARCHER_FORBIDDEN_COLUMN_PLAYER_SIDE;

@Getter
@Setter
public class Field {

    private List<UnitSpot> field;

    public Field() {
        field = new ArrayList<>(FIELD_CAPACITY);
        for (int row = 1; row <= 4; row++) {
            for (int column = 1; column <= 3; column++) {
                field.add(new UnitSpot(row, column));
            }
        }
    }

    public boolean isSpotAvailable(UnitDto unit, int row, int column) {
        if (!canUnitTypeBePlacedHere(unit, column)) {
            return false;
        }

        UnitSpot spotToCheck = getUnitSpot(row, column);
        return spotToCheck.isAvailable();
    }

    public void setUnitSpot(int row, int column, UnitDto unit) {
        UnitSpot unitSpot = getUnitSpot(row, column);
        unitSpot.setUnit(unit);
        unitSpot.setAvailable(false);
    }

    public UnitSpot getUnitSpot(int row, int column) {
        return field.stream()
                .filter(us -> us.getRow() == row)
                .filter(us -> us.getColumn() == column)
                .findFirst().get();
    }

    private boolean canUnitTypeBePlacedHere(UnitDto unit, int column) {
        switch (unit.getType()) {
            case ARCHER:
            case MAGE:
                if (column == MAGE_OR_ARCHER_FORBIDDEN_COLUMN_PLAYER_SIDE) {
                    System.out.println("Given unit cannot be placed here!");
                    return false;
                } else {
                    return true;
                }
            case KNIGHT:
            case WARRIOR:
                if (column == KNIGHT_OR_WARRIOR_FORBIDDEN_COLUMN_PLAYER_SIDE) {
                    System.out.println("Given unit cannot be placed here!");
                    return false;
                } else {
                    return true;
                }
            default:
                return false;
        }
    }

    public List<UnitSpot> getAllOccupiedSpotsPerRow(int row, boolean isPlayerTurn, boolean isAttackerField) {
        return field.stream()
                .filter(unitSpot -> unitSpot.getRow() == row)
                .filter(unitSpot -> !unitSpot.isAvailable())
                .sorted(getUnitSpotColumnComparator(isPlayerTurn, isAttackerField))
                .collect(Collectors.toList());
    }

    private Comparator<UnitSpot> getUnitSpotColumnComparator(boolean isPlayerTurn, boolean isAttackerField) {
        if ((isPlayerTurn && isAttackerField) || (!isPlayerTurn && !isAttackerField)) {
            return Comparator.comparing(UnitSpot::getColumn, Comparator.reverseOrder());
        } else {
            return Comparator.comparing(UnitSpot::getColumn);
        }
    }

    public List<UnitSpot> getAllAvailableSpots() {
        return field.stream()
                .filter(UnitSpot::isAvailable)
                .collect(Collectors.toList());
    }

    public void makeUnitSpotEmpty(int row, int column) {
        UnitSpot chosenUnitSpot = field.stream()
                .filter(unitSpot -> unitSpot.getRow() == row)
                .filter(unitSpot -> unitSpot.getColumn() == column)
                .findFirst()
                .get();

        chosenUnitSpot.setUnit(null);
        chosenUnitSpot.setAvailable(true);
    }
}
