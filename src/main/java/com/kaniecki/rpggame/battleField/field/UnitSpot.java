package com.kaniecki.rpggame.battleField.field;

import com.kaniecki.rpggame.unit.dto.UnitDto;
import lombok.Data;

@Data
public class UnitSpot {

    private boolean isAvailable;
    private UnitDto unit;
    private Integer row;
    private Integer column;

    public UnitSpot(Integer row, Integer column) {
        this.row = row;
        this.column = column;
        this.unit = null;
        this.isAvailable = true;
    }

    @Override
    public String toString() {
        if (unit != null) {
            return unit.toString();
        } else {
            return "";
        }
    }
}
