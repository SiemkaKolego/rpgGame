package com.kaniecki.rpggame.battleField.service;

import com.kaniecki.rpggame.battleField.field.Field;
import com.kaniecki.rpggame.battleField.field.UnitSpot;
import com.kaniecki.rpggame.battleField.service.printer.BattleFieldPrinter;
import com.kaniecki.rpggame.box.dto.BoxDto;
import com.kaniecki.rpggame.box.service.IBoxService;
import com.kaniecki.rpggame.common.unit.Type;
import com.kaniecki.rpggame.simulator.EnemySimulator;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import com.kaniecki.rpggame.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import static com.kaniecki.rpggame.common.unit.UnitConstantValues.UNIT_DECK_FOR_BATTLE_TIME;

@Component
public class BattleFieldImpl implements IBattleField {

    @Autowired
    private EnemySimulator enemySimulator;

    @Autowired
    private BattleFieldPrinter printer;

    //TODO - move this to separate view for starting battles and opening boxes
    @Autowired
    private IBoxService boxService;

    private UserDto player;
    private UserDto enemy;

    private Field playerField;
    private Field enemyField;

    private List<UnitDto> playerUnitDeckForBattleTime;
    private List<UnitDto> enemyUnitDeckForBattleTime;

    private int round;

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public void performBattle(UserDto player, UserDto enemy) {
        initializeClassFields(player, enemy);

        initializeUnitDeckForBattle(playerUnitDeckForBattleTime, player);
        initializeUnitDeckForBattle(enemyUnitDeckForBattleTime, enemy);

        boolean isEnemyDead = false;
        boolean isPlayerDead = false;

        while (!isEnemyDead || !isPlayerDead) {

            System.out.println("===============> NEW TURN <===============");
            System.out.println(printBattleField(true, false));

            isEnemyDead = performPlayerTurn(enemy);
            if (isEnemyDead) {
                System.out.println(printBattleField(false, false));
                System.out.println("Player wins!");
                clearClassFields();
                addReward(player);
                break;
            }

            isPlayerDead = performEnemyTurn(player);
            if (isPlayerDead) {
                System.out.println("Player loose!");
                clearClassFields();
                break;
            }

            addRandomUnitToDeck(playerUnitDeckForBattleTime, player.getUnitsInDeck());
            addRandomUnitToDeck(enemyUnitDeckForBattleTime, enemy.getUnitsInDeck());

            round++;
        }
    }

    private void clearClassFields() {
        playerField = null;
        enemyField = null;

        playerUnitDeckForBattleTime = null;
        enemyUnitDeckForBattleTime = null;

        this.player = null;
        this.enemy = null;

        round = 0;
    }

    private void initializeClassFields(UserDto player, UserDto enemy) {
        playerField = new Field();
        enemyField = new Field();

        playerUnitDeckForBattleTime = new ArrayList<>(UNIT_DECK_FOR_BATTLE_TIME);
        enemyUnitDeckForBattleTime = new ArrayList<>(UNIT_DECK_FOR_BATTLE_TIME);

        this.player = player;
        this.enemy = enemy;

        round = 1;
    }

    private void initializeUnitDeckForBattle(List<UnitDto> unitDeckForBattleTime, UserDto user) {
        for (int i = 0; i < 4; i++) {
            addRandomUnitToDeck(unitDeckForBattleTime, user.getUnitsInDeck());
        }
    }

    private void addRandomUnitToDeck(List<UnitDto> deckToUpdate, Set<UnitDto> sourceDeck) {
        int randomUnitIndex = new Random().nextInt(sourceDeck.size());
        int index = 0;

        for (UnitDto unit : sourceDeck) {
            if (randomUnitIndex == index) {
                deckToUpdate.add(copyUnit(unit));
            }
            index++;
        }
    }

    private UnitDto copyUnit(UnitDto source) {
        return new UnitDto(source);
    }

    private boolean performPlayerTurn(UserDto opponent) {
        UnitDto chosenUnit = getUnitFromPlayer();
        setUnitSpotForPlayer(chosenUnit);
        System.out.println("PLAYER TURN - " + round);
        System.out.println(printBattleField(true, true));
        return attackOpponent(playerField, enemyField, opponent, true);
    }

    private UnitDto getUnitFromPlayer() {
        System.out.println("Enter unit number you want to chose: ");
        int unitNumber = scanner.nextInt();
        return playerUnitDeckForBattleTime.remove(unitNumber - 1);
    }

    private void setUnitSpotForPlayer(UnitDto unit) {
        System.out.println("Enter row number you want to place unit: ");
        int row = scanner.nextInt();
        System.out.println("Enter column number you want to place unit: ");
        int column = scanner.nextInt();

        if (playerField.isSpotAvailable(unit, row, column)) {
            playerField.setUnitSpot(row, column, unit);
        } else {
            setUnitSpotForPlayer(unit);
        }
    }

    private boolean performEnemyTurn(UserDto opponent) {
        UnitDto chosenUnit = getRandomUnitFromEnemysDeck();
        setRandomUnitSpotForEnemy(chosenUnit);
        System.out.println("ENEMY TURN - " + round);
        System.out.println(printBattleField(true, true));
        return attackOpponent(enemyField, playerField, opponent, false);
    }

    private UnitDto getRandomUnitFromEnemysDeck() {
        Random random = new Random();
        return enemyUnitDeckForBattleTime.remove(random.nextInt(enemyUnitDeckForBattleTime.size()));
    }

    private void setRandomUnitSpotForEnemy(UnitDto chosenUnit) {
        enemySimulator.setRandomUnitSpot(chosenUnit, enemyField);
    }

    private boolean attackOpponent(Field attackerField, Field defenderField, UserDto opponent, boolean isPlayerTurn) {
        boolean isOpponentDead = false;

        for (int i = 1; i <= 4; i++) {
            if (performUnitsAttackForRow(i, attackerField, defenderField, opponent, isPlayerTurn)) {
                isOpponentDead = true;
                break;
            }
        }

        return isOpponentDead;
    }

    private boolean performUnitsAttackForRow(int row, Field attackerField, Field defenderField, UserDto opponent, boolean isPlayerTurn) {
        List<UnitSpot> attackerSideSpotsWithUnit = attackerField.getAllOccupiedSpotsPerRow(row, isPlayerTurn, true);
        List<UnitSpot> defenderSideSpotsWithUnit = defenderField.getAllOccupiedSpotsPerRow(row, isPlayerTurn, false);
        boolean isOpponentDead = false;

        if (!attackerSideSpotsWithUnit.isEmpty()) {
            if (!defenderSideSpotsWithUnit.isEmpty()) {
                isOpponentDead = calculateUnitHealthDecrease(attackerSideSpotsWithUnit, defenderSideSpotsWithUnit, defenderField, opponent);
            } else {
                ListIterator<UnitSpot> attackerSpotIterator = attackerSideSpotsWithUnit.listIterator();

                while (attackerSpotIterator.hasNext()) {
                    UnitSpot attackerSpot = getAttackerSpotIfCanAttack(attackerSpotIterator);

                    if (attackerSpot == null) {
                        continue;
                    }

                    isOpponentDead = decreaseDirectlyUserHealthAndCheckIfIsDead(attackerSpot, opponent);
                }
            }
        }

        return isOpponentDead;
    }

    private boolean calculateUnitHealthDecrease(List<UnitSpot> attackerSideSpotsWithUnit,
                                                List<UnitSpot> defenderSideSpotsWithUnit,
                                                Field defenderField, UserDto opponent) {
        ListIterator<UnitSpot> attackerSpotIterator = attackerSideSpotsWithUnit.listIterator();
        ListIterator<UnitSpot> defenderSpotIterator = defenderSideSpotsWithUnit.listIterator();

        boolean isOpponentDead = false;
        Boolean shouldTakeNextDefenderUnit = true;
        boolean shouldEndTurn = false;
        UnitSpot defenderSpot = null;

        while (!shouldEndTurn) {

            if (!attackerSpotIterator.hasNext()) {
                shouldEndTurn = true;
                continue;
            }

            if (shouldTakeNextDefenderUnit != null && shouldTakeNextDefenderUnit && defenderSpotIterator.hasNext() && attackerSpotIterator.hasNext()) {
                defenderSpot = defenderSpotIterator.next();
                UnitSpot attackerSpot = getAttackerSpotIfCanAttack(attackerSpotIterator);

                if (attackerSpot == null) {
                    continue;
                }

                int defenderUnitRemainingHealth = defenderSpot.getUnit().getHealth() - attackerSpot.getUnit().getAttack();

                if (defenderUnitRemainingHealth > 0) {
                    defenderSpot.getUnit().setHealth(defenderUnitRemainingHealth);
                    shouldTakeNextDefenderUnit = false;
                } else {
                    setToEmptyUnitSpot(defenderField, defenderSpot);

                    if (!defenderSpotIterator.hasNext()) {
                        shouldTakeNextDefenderUnit = null;
                    } else {
                        shouldTakeNextDefenderUnit = true;
                    }
                }

                continue;
            }

            if (shouldTakeNextDefenderUnit != null && !shouldTakeNextDefenderUnit && attackerSpotIterator.hasNext()) {
                UnitSpot attackerSpot = getAttackerSpotIfCanAttack(attackerSpotIterator);

                if (attackerSpot == null) {
                    continue;
                }

                int defenderUnitRemainingHealth = defenderSpot.getUnit().getHealth() - attackerSpot.getUnit().getAttack();

                if (defenderUnitRemainingHealth > 0) {
                    defenderSpot.getUnit().setHealth(defenderUnitRemainingHealth);
                } else {
                    setToEmptyUnitSpot(defenderField, defenderSpot);

                    if (!defenderSpotIterator.hasNext()) {
                        shouldTakeNextDefenderUnit = null;
                    } else {
                        shouldTakeNextDefenderUnit = true;
                    }
                }
                continue;
            }

            if (shouldTakeNextDefenderUnit == null && attackerSpotIterator.hasNext()) {
                while (attackerSpotIterator.hasNext() && !isOpponentDead) {
                    UnitSpot attackerSpot = getAttackerSpotIfCanAttack(attackerSpotIterator);

                    if (attackerSpot == null) {
                        continue;
                    }

                    isOpponentDead = decreaseDirectlyUserHealthAndCheckIfIsDead(attackerSpot, opponent);
                }

                shouldEndTurn = true;
            }
        }

        return isOpponentDead;
    }

    private UnitSpot getAttackerSpotIfCanAttack(ListIterator<UnitSpot> attackerSpotIterator) {
        if (attackerSpotIterator.hasPrevious()) {
            UnitSpot currentAttackerSpot = attackerSpotIterator.next();
            Type type = currentAttackerSpot.getUnit().getType();

            if (Type.WARRIOR.equals(type) || Type.KNIGHT.equals(type)) {
                return null;
            } else {
                return currentAttackerSpot;
            }
        }

        return attackerSpotIterator.next();
    }

    private void setToEmptyUnitSpot(Field field, UnitSpot unitSpot) {
        field.makeUnitSpotEmpty(unitSpot.getRow(), unitSpot.getColumn());
    }

    private boolean decreaseDirectlyUserHealthAndCheckIfIsDead(UnitSpot attackerSpot, UserDto opponent) {
        opponent.setHealth(opponent.getHealth() - attackerSpot.getUnit().getAttack());
        return opponent.getHealth() < 0;
    }

    private String printBattleField(boolean printDeck, boolean beforeDeckFill) {
        StringBuilder sb = new StringBuilder();

        sb.append(printer.printBattleField(player.getHealth(), enemy.getHealth(), playerField, enemyField));
        sb.append("\n");

        if (printDeck) {
            sb.append(printer.printUnitsInPlayersDeck(playerUnitDeckForBattleTime, beforeDeckFill));
            sb.append("\n");
        }

        return sb.toString();
    }

    private void addReward(UserDto player) {
        BoxDto box = boxService.generateBoxForUser(player.getAllUnitsAvailable());

        //TODO - just for test purposes, should be removed when time-box will be added
        System.out.println("===========> Player current gold and unit experience <===========");
        System.out.println("Gold: " + player.getGold());
        for (UnitDto unit : player.getAllUnitsAvailable()) {
            System.out.println("Name: " + unit.getName() + ", currentExp: " + unit.getCurrentExperience() + ", special abilities: " + getUnitSpecialAbilitiesSize(unit));
        }

        boxService.openBoxAndGrantRewardsToUser(player, box);

        //TODO - just for test purposes, should be removed when time-box will be added
        System.out.println("===========> Player gold and unit experience after opening box <===========");
        System.out.println("Gold: " + player.getGold());
        for (UnitDto unit : player.getAllUnitsAvailable()) {
            System.out.println("Name: " + unit.getName() + ", currentExp: " + unit.getCurrentExperience() + ", special abilities: " + getUnitSpecialAbilitiesSize(unit));
        }
    }

    private int getUnitSpecialAbilitiesSize(UnitDto unit) {
        if (unit.getSpecialAbilities() == null) {
            return 0;
        } else {
            return unit.getSpecialAbilities().size();
        }
    }
}
