package com.kaniecki.rpggame.battleField.service.printer;

import com.kaniecki.rpggame.battleField.field.Field;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BattleFieldPrinter {

    public String printBattleField(int playerHealth, int defenderHealth,
                                   Field playerSide, Field defenderSide) {
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        AT_Row rowWithHealth = asciiTable.addRow(null, null, null, "Player: " + playerHealth, null, null, "Defender: " + defenderHealth);
        asciiTable.addRule();
        AT_Row rowWithColumnNumber = asciiTable.addRow(" ", "1", "2", "3", "3", "2", "1");
        asciiTable.addRule();
        asciiTable.addRow(getRow(1, playerSide, defenderSide));
        asciiTable.addRule();
        asciiTable.addRow(getRow(2, playerSide, defenderSide));
        asciiTable.addRule();
        asciiTable.addRow(getRow(3, playerSide, defenderSide));
        asciiTable.addRule();
        asciiTable.addRow(getRow(4, playerSide, defenderSide));
        asciiTable.addRule();

        rowWithHealth.setTextAlignment(TextAlignment.CENTER);
        rowWithColumnNumber.setTextAlignment(TextAlignment.CENTER);

        return asciiTable.render();
    }

    public String printUnitsInPlayersDeck(List<UnitDto> unitsDeck, boolean beforeDeckFill) {
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();

        if (beforeDeckFill) {
            asciiTable.addRow("Number 1", "Number 2", "Number 3");
        } else {
            asciiTable.addRow("Number 1", "Number 2", "Number 3", "Number 4");
        }

        asciiTable.addRow(getUnitsName(unitsDeck));
        asciiTable.addRow(getUnitsHealth(unitsDeck));
        asciiTable.addRow(getUnitsAttack(unitsDeck));
        asciiTable.addRule();

        return asciiTable.render();
    }

    private List<String> getUnitsName(List<UnitDto> unitsDeck) {
        List<String> unitName = new ArrayList<>();

        for (UnitDto unit : unitsDeck) {
            unitName.add(unit.getName());
        }

        return unitName;
    }

    private List<String> getUnitsHealth(List<UnitDto> unitsDeck) {
        List<String> unitHealth = new ArrayList<>();

        for (UnitDto unit : unitsDeck) {
            unitHealth.add("H: " + unit.getHealth());
        }

        return unitHealth;
    }

    private List<String> getUnitsAttack(List<UnitDto> unitsDeck) {
        List<String> unitAttack = new ArrayList<>();

        for (UnitDto unit : unitsDeck) {
            unitAttack.add("A: " + unit.getAttack());
        }

        return unitAttack;
    }

    private List<String> getRow(int rowNumber, Field playerSide, Field defenderSide) {
        List<String> row = new ArrayList<>();

        row.add(rowNumber + "");
        row.add(playerSide.getUnitSpot(rowNumber, 1).toString());
        row.add(playerSide.getUnitSpot(rowNumber, 2).toString());
        row.add(playerSide.getUnitSpot(rowNumber, 3).toString());
        row.add(defenderSide.getUnitSpot(rowNumber, 1).toString());
        row.add(defenderSide.getUnitSpot(rowNumber, 2).toString());
        row.add(defenderSide.getUnitSpot(rowNumber, 3).toString());

        return row;
    }
}
