package com.kaniecki.rpggame.battleField.service;

import com.kaniecki.rpggame.user.dto.UserDto;

public interface IBattleField {

    void performBattle(UserDto user, UserDto opponent);
}
