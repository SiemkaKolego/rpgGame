package com.kaniecki.rpggame.simulator;

import com.kaniecki.rpggame.battleField.field.Field;
import com.kaniecki.rpggame.battleField.field.UnitSpot;
import com.kaniecki.rpggame.common.unit.Type;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.kaniecki.rpggame.common.battleField.BattleFieldConstantValues.KNIGHT_OR_WARRIOR_FORBIDDEN_COLUMN_ENEMY_SIDE;
import static com.kaniecki.rpggame.common.battleField.BattleFieldConstantValues.MAGE_OR_ARCHER_FORBIDDEN_COLUMN_ENEMY_SIDE;

@Component
public class EnemySimulator {

    public void setRandomUnitSpot(UnitDto chosenUnit, Field enemyField) {
        //TODO increase logic here to for example put unit in line where already player put its own
        // or check if you can kill already places player unit by checking units health etc
        Type unitType = chosenUnit.getType();
        int columnToIgnore = getColumnToIgnoreBasedOnUnitType(unitType);

        List<UnitSpot> allAvailableUnitSpots = enemyField.getAllAvailableSpots();
        List<UnitSpot> availableSpotsForGivenType = getAvailableSpotsForGivenType(allAvailableUnitSpots, columnToIgnore);

        UnitSpot randomUnitSpot = chooseUnitSpot(availableSpotsForGivenType);
        enemyField.setUnitSpot(randomUnitSpot.getRow(), randomUnitSpot.getColumn(), chosenUnit);
    }

    private UnitSpot chooseUnitSpot(List<UnitSpot> availableSpotsForGivenType) {
        Random random = new Random();
        return availableSpotsForGivenType.get(random.nextInt(availableSpotsForGivenType.size()));
    }

    private int getColumnToIgnoreBasedOnUnitType(Type unitType) {
        if (Type.MAGE.equals(unitType) || Type.ARCHER.equals(unitType)) {
            return MAGE_OR_ARCHER_FORBIDDEN_COLUMN_ENEMY_SIDE;
        } else {
            return KNIGHT_OR_WARRIOR_FORBIDDEN_COLUMN_ENEMY_SIDE;
        }
    }

    private List<UnitSpot> getAvailableSpotsForGivenType(List<UnitSpot> allAvailableUnitSpots, int columnToIgnore) {
        return allAvailableUnitSpots.stream()
                .filter(unitSpot -> unitSpot.getColumn() != columnToIgnore)
                .collect(Collectors.toList());
    }
}
