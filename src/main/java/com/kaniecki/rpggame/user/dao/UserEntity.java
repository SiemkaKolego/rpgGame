package com.kaniecki.rpggame.user.dao;

import com.kaniecki.rpggame.box.dao.BoxEntity;
import com.kaniecki.rpggame.unit.dao.UnitEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.kaniecki.rpggame.common.box.BoxConstantValues.USER_BOXES_MAX_CAPACITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.UNITS_DECK_MAX_CAPACITY;

@Data
public class UserEntity {

    private String name;
    private int health;
    private Set<UnitEntity> allUnitsAvailable;
    private Set<UnitEntity> unitsInDeck = new HashSet<>(UNITS_DECK_MAX_CAPACITY);
    private List<BoxEntity> boxes = new ArrayList<>(USER_BOXES_MAX_CAPACITY);
    private int gold;
}
