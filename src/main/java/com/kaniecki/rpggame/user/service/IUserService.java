package com.kaniecki.rpggame.user.service;

import com.kaniecki.rpggame.user.dto.UserDto;

public interface IUserService {

    void createUser(UserDto user);
}
