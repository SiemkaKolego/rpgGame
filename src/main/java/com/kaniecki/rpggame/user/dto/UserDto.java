package com.kaniecki.rpggame.user.dto;

import com.kaniecki.rpggame.box.dto.BoxDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.kaniecki.rpggame.common.box.BoxConstantValues.USER_BOXES_MAX_CAPACITY;

@Data
public class UserDto {

    private String name;
    private int health;
    private Set<UnitDto> allUnitsAvailable;
    private Set<UnitDto> unitsInDeck;
    private List<BoxDto> boxes = new ArrayList<>(USER_BOXES_MAX_CAPACITY);
    private int gold;
}
