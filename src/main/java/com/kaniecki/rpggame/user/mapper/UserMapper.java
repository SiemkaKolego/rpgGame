package com.kaniecki.rpggame.user.mapper;

import com.kaniecki.rpggame.user.dao.UserEntity;
import com.kaniecki.rpggame.user.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserEntity userDtoToUserEntity(UserDto source);
    UserDto userEntityToUserDto(UserEntity source);
}
