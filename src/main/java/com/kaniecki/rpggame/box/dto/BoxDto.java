package com.kaniecki.rpggame.box.dto;

import com.kaniecki.rpggame.unit.dto.UnitDto;
import lombok.Data;

@Data
public class BoxDto {

    private int gold;
    private UnitDto unit;
    private int unitExperience;
}
