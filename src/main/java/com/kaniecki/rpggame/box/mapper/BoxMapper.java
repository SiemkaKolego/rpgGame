package com.kaniecki.rpggame.box.mapper;

import com.kaniecki.rpggame.box.dao.BoxEntity;
import com.kaniecki.rpggame.box.dto.BoxDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BoxMapper {

    BoxEntity boxDtoToBoxEntity(BoxDto source);
    BoxDto boxEntityToBoxDto(BoxEntity source);
}
