package com.kaniecki.rpggame.box.dao;

import com.kaniecki.rpggame.unit.dao.UnitEntity;
import lombok.Data;

@Data
public class BoxEntity {

    private int gold;
    private UnitEntity unit;
    private int unitExperience;
}
