package com.kaniecki.rpggame.box.service;

import com.kaniecki.rpggame.box.dto.BoxDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import com.kaniecki.rpggame.user.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.Set;

import static com.kaniecki.rpggame.common.box.BoxConstantValues.GOLD_MAX;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.GOLD_MIN;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.UNIT_EXPERIENCE_MAX;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.UNIT_EXPERIENCE_MIN;

@Service
public class BoxServiceImpl implements IBoxService {

    @Override
    public BoxDto generateBoxForUser(Set<UnitDto> userAvailableUnits) {
        Random random = new Random();

        int unitExperience = random.nextInt((UNIT_EXPERIENCE_MAX - UNIT_EXPERIENCE_MIN) + 1) + UNIT_EXPERIENCE_MIN;
        int gold = random.nextInt((GOLD_MAX - GOLD_MIN) + 1) + GOLD_MIN;
        UnitDto unit = getRandomUnit(userAvailableUnits);

        if (unit == null) {
            System.out.println("No unit found for box reward!");
            //TODO - throw error / error management
        }

        return createBox(unit, unitExperience, gold);
    }

    private BoxDto createBox(UnitDto unit, int unitExperience, int gold) {
        BoxDto box = new BoxDto();

        box.setUnit(unit);
        box.setUnitExperience(unitExperience);
        box.setGold(gold);

        return box;
    }

    @Override
    public void openBoxAndGrantRewardsToUser(UserDto user, BoxDto box) {
        user.setGold(user.getGold() + box.getGold());

        UnitDto unit = findUnit(user.getAllUnitsAvailable(), box.getUnit());

        if (unit == null) {
            System.out.println("No unit found in user all units that would match box reward unit!");
            //TODO - throw error / error management
            return;
        }

        unit.setCurrentExperience(unit.getCurrentExperience() + box.getUnitExperience());
    }

    private UnitDto findUnit(Set<UnitDto> allUnitsAvailable, UnitDto unitToUpdate) {
        for (UnitDto unit : allUnitsAvailable) {
            if (unit.equals(unitToUpdate)) {
                return unit;
            }
        }

        return null;
    }

    private UnitDto getRandomUnit(Set<UnitDto> userAvailableUnits) {
        int randomUnitIndex = new Random().nextInt(userAvailableUnits.size());
        int index = 0;

        for (UnitDto unit : userAvailableUnits) {
            if (randomUnitIndex == index) {
                return unit;
            }
            index++;
        }

        return null;
    }
}
