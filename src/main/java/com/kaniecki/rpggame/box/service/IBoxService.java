package com.kaniecki.rpggame.box.service;

import com.kaniecki.rpggame.box.dto.BoxDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import com.kaniecki.rpggame.user.dto.UserDto;

import java.util.Set;

public interface IBoxService {

    BoxDto generateBoxForUser(Set<UnitDto> userAvailableUnits);
    void openBoxAndGrantRewardsToUser(UserDto user, BoxDto box);
}
