package com.kaniecki.rpggame.box.service;

import com.kaniecki.rpggame.box.dto.BoxDto;
import com.kaniecki.rpggame.common.unit.Rarity;
import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import com.kaniecki.rpggame.common.unit.Type;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import com.kaniecki.rpggame.unit.service.UnitServiceTestUtils;
import com.kaniecki.rpggame.user.dto.UserDto;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static com.kaniecki.rpggame.common.unit.UnitConstantValues.FIRST_SPECIAL_ABILITY_INITIAL_VALUE;

public class BoxServiceTestUtils {

    public static final int DEFAULT_INITIAL_EXPERIENCE = 0;
    public static final int DEFAULT_INITIAL_GOLD = 0;

    public static Set<UnitDto> generateAvailableUnits() {
        Set<UnitDto> availableUnits = new HashSet<>();

        availableUnits.add(createCommonKnight());
        availableUnits.add(createRareArcher());

        return availableUnits;
    }

    public static UserDto createUser(Set<UnitDto> availableUnits) {
        UserDto user = new UserDto();

        user.setGold(DEFAULT_INITIAL_GOLD);
        user.setAllUnitsAvailable(availableUnits);

        return user;
    }

    public static BoxDto createBox() {
        BoxDto box = new BoxDto();

        //simulate randomness
        Random random = new Random();
        int randomNumber = random.nextInt(2);

        box.setUnit(randomNumber == 0 ? createCommonKnight() : createRareArcher());
        box.setUnitExperience(20);
        box.setGold(30);

        return box;
    }

    private static UnitDto createCommonKnight() {
        return UnitServiceTestUtils.createUnit(
                Rarity.COMMON,
                null,
                Type.KNIGHT,
                1,
                DEFAULT_INITIAL_EXPERIENCE,
                50,
                1
        );
    }

    private static UnitDto createRareArcher() {
        return UnitServiceTestUtils.createUnit(
                Rarity.RARE,
                UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), FIRST_SPECIAL_ABILITY_INITIAL_VALUE),
                Type.ARCHER,
                3,
                DEFAULT_INITIAL_EXPERIENCE,
                150,
                2
        );
    }
}
