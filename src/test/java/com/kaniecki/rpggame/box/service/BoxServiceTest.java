package com.kaniecki.rpggame.box.service;

import com.kaniecki.rpggame.box.dto.BoxDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import com.kaniecki.rpggame.user.dto.UserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

import static com.kaniecki.rpggame.box.service.BoxServiceTestUtils.DEFAULT_INITIAL_EXPERIENCE;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.GOLD_MAX;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.GOLD_MIN;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.UNIT_EXPERIENCE_MAX;
import static com.kaniecki.rpggame.common.box.BoxConstantValues.UNIT_EXPERIENCE_MIN;

@SpringBootTest
public class BoxServiceTest {

    @Autowired
    private IBoxService boxService;

    @Test
    public void testGenerateBoxForUser() {
        //Given
        Set<UnitDto> availableUnits = BoxServiceTestUtils.generateAvailableUnits();

        //When
        BoxDto generatedBox = boxService.generateBoxForUser(availableUnits);

        //Then
        Assertions.assertNotNull(generatedBox);
        Assertions.assertTrue(generatedBox.getGold() >= GOLD_MIN && generatedBox.getGold() <= GOLD_MAX);
        Assertions.assertTrue(generatedBox.getUnitExperience() >= UNIT_EXPERIENCE_MIN && generatedBox.getUnitExperience() <= UNIT_EXPERIENCE_MAX);
        Assertions.assertTrue(availableUnits.contains(generatedBox.getUnit()));
    }

    //TODO - add test where no unit was fond from available units set (when error management will be created)

    @Test
    public void testOpenBoxAndGrantRewardsToUser() {
        //Given
        Set<UnitDto> availableUnits = BoxServiceTestUtils.generateAvailableUnits();
        UserDto user = BoxServiceTestUtils.createUser(availableUnits);
        BoxDto box = BoxServiceTestUtils.createBox();

        //When
        boxService.openBoxAndGrantRewardsToUser(user, box);

        //Then
        Assertions.assertTrue(user.getGold() >= GOLD_MIN && user.getGold() <= GOLD_MAX);
        Assertions.assertTrue(oneOfUserUnitHasExperienceIncreased(user));
    }

    //TODO - add test where no unit was fond from available units set or when unit from box doesn't match with any unit from user (when error management will be created)

    private boolean oneOfUserUnitHasExperienceIncreased(UserDto user) {
        boolean hasExperienceIncreased = false;

        for (UnitDto unit : user.getAllUnitsAvailable()) {
            if (unit.getCurrentExperience() > DEFAULT_INITIAL_EXPERIENCE) {
                hasExperienceIncreased = true;
                break;
            }
        }

        return hasExperienceIncreased;
    }
}
