package com.kaniecki.rpggame.unit.service;

import com.kaniecki.rpggame.common.unit.Rarity;
import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import com.kaniecki.rpggame.common.unit.Type;
import com.kaniecki.rpggame.unit.dto.SpecialAbilityDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;

import java.util.ArrayList;
import java.util.List;

public class UnitServiceTestUtils {

    protected static final String DEFAULT_NAME = "Test";
    protected static final int DEFAULT_ATTACK = 1;
    protected static final int DEFAULT_HEALTH = 1;

    public static UnitDto createUnit(Rarity rarity, List<SpecialAbilityDto> specialAbilities, Type type,
                                 int currentLevel, int currentExperience, int nextLevelExperience, int specialAbilitiesMaxQuantity) {
        UnitDto unit = new UnitDto();
        unit.setHealth(DEFAULT_HEALTH);
        unit.setAttack(DEFAULT_ATTACK);
        unit.setName(DEFAULT_NAME);
        unit.setLevel(currentLevel);
        unit.setCurrentExperience(currentExperience);
        unit.setNextLevelExperience(nextLevelExperience);
        unit.setRarity(rarity);
        unit.setType(type);
        unit.setSpecialAbilitiesMaxQuantity(specialAbilitiesMaxQuantity);

        if (specialAbilities != null) {
            unit.setSpecialAbilities(specialAbilities);
        }

        return unit;
    }

    public static List<SpecialAbilityDto> createSpecialAbilities(List<SpecialAbilityType> specialAbilityTypes, int value) {
        List<SpecialAbilityDto> specialAbilities = new ArrayList<>();

        for (SpecialAbilityType specialAbilityType : specialAbilityTypes) {
            SpecialAbilityDto specialAbilityDto = new SpecialAbilityDto(specialAbilityType, value);
            specialAbilities.add(specialAbilityDto);
        }

        return specialAbilities;
    }
}
