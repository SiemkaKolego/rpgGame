package com.kaniecki.rpggame.unit.service;

import com.kaniecki.rpggame.common.unit.Rarity;
import com.kaniecki.rpggame.common.unit.SpecialAbilityType;
import com.kaniecki.rpggame.common.unit.Type;
import com.kaniecki.rpggame.unit.dto.SpecialAbilityDto;
import com.kaniecki.rpggame.unit.dto.UnitDto;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.kaniecki.rpggame.common.unit.UnitConstantValues.ATTACK_INCREMENT_VALUE_FOR_ARCHER;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.ATTACK_INCREMENT_VALUE_FOR_KNIGHT;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.ATTACK_INCREMENT_VALUE_FOR_MAGE;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.ATTACK_INCREMENT_VALUE_FOR_WARRIOR;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.FIRST_SPECIAL_ABILITY_INITIAL_VALUE;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.HEALTH_INCREMENT_VALUE_FOR_ARCHER;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.HEALTH_INCREMENT_VALUE_FOR_KNIGHT;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.HEALTH_INCREMENT_VALUE_FOR_MAGE;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.HEALTH_INCREMENT_VALUE_FOR_WARRIOR;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.NEXT_SPECIAL_ABILITY_INITIAL_VALUE;
import static com.kaniecki.rpggame.common.unit.UnitConstantValues.SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class UnitServiceTest {

    @Autowired
    private IUnitService unitService;
    
    //TODO - change values of experience/healt/attak to the real one matching current level

    @Test
    public void testLevelUpCommonKnight() {
        //given
        UnitDto commonKnight =
                UnitServiceTestUtils.createUnit(Rarity.COMMON, null, Type.KNIGHT, 1, 3, 2, 0);

        //when
        unitService.levelUp(commonKnight);

        //then
        assertEquals(2, commonKnight.getLevel());
        assertEquals(1, commonKnight.getCurrentExperience());
        assertEquals(18, commonKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, commonKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, commonKnight.getHealth());
        assertNull(commonKnight.getSpecialAbilities());
    }

    @Test
    public void testLevelUpCommonWarrior() {
        //given
        UnitDto commonWarrior =
                UnitServiceTestUtils.createUnit(Rarity.COMMON, null, Type.WARRIOR, 1, 3, 2, 0);

        //when
        unitService.levelUp(commonWarrior);

        //then
        assertEquals(2, commonWarrior.getLevel());
        assertEquals(1, commonWarrior.getCurrentExperience());
        assertEquals(18, commonWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, commonWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, commonWarrior.getHealth());
        assertNull(commonWarrior.getSpecialAbilities());
    }

    @Test
    public void testLevelUpCommonArcher() {
        //given
        UnitDto commonArcher =
                UnitServiceTestUtils.createUnit(Rarity.COMMON, null, Type.ARCHER, 1, 3, 2, 0);

        //when
        unitService.levelUp(commonArcher);

        //then
        assertEquals(2, commonArcher.getLevel());
        assertEquals(1, commonArcher.getCurrentExperience());
        assertEquals(18, commonArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, commonArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, commonArcher.getHealth());
        assertNull(commonArcher.getSpecialAbilities());
    }

    @Test
    public void testLevelUpCommonMage() {
        //given
        UnitDto commonMage =
                UnitServiceTestUtils.createUnit(Rarity.COMMON, null, Type.MAGE, 1, 3, 2, 0);

        //when
        unitService.levelUp(commonMage);

        //then
        assertEquals(2, commonMage.getLevel());
        assertEquals(1, commonMage.getCurrentExperience());
        assertEquals(18, commonMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, commonMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, commonMage.getHealth());
        assertNull(commonMage.getSpecialAbilities());
    }

    @Test
    public void testLevelUpRareKnightThatShouldGetNewSpecialAbility() {
        //given
        UnitDto rareKnight =
                UnitServiceTestUtils.createUnit(Rarity.RARE, null, Type.KNIGHT, 4, 3, 2, 1);

        //when
        unitService.levelUp(rareKnight);

        //then
        assertEquals(5, rareKnight.getLevel());
        assertEquals(1, rareKnight.getCurrentExperience());
        assertEquals(135, rareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, rareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, rareKnight.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE), rareKnight.getSpecialAbilities());
    }

    @Test
    public void testLevelUpRareWarriorThatShouldGetNewSpecialAbility() {
        //given
        UnitDto rareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.RARE, null, Type.WARRIOR, 4, 3, 2, 1);

        //when
        unitService.levelUp(rareWarrior);

        //then
        assertEquals(5, rareWarrior.getLevel());
        assertEquals(1, rareWarrior.getCurrentExperience());
        assertEquals(135, rareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, rareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, rareWarrior.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM), FIRST_SPECIAL_ABILITY_INITIAL_VALUE), rareWarrior.getSpecialAbilities());
    }

    @Test
    public void testLevelUpRareArcherThatShouldGetNewSpecialAbility() {
        //given
        UnitDto rareArcher =
                UnitServiceTestUtils.createUnit(Rarity.RARE, null, Type.ARCHER, 4, 3, 2, 1);

        //when
        unitService.levelUp(rareArcher);

        //then
        assertEquals(5, rareArcher.getLevel());
        assertEquals(1, rareArcher.getCurrentExperience());
        assertEquals(135, rareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, rareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, rareArcher.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), FIRST_SPECIAL_ABILITY_INITIAL_VALUE), rareArcher.getSpecialAbilities());
    }

    @Test
    public void testLevelUpRareMageThatShouldGetNewSpecialAbility() {
        //given
        UnitDto rareMage =
                UnitServiceTestUtils.createUnit(Rarity.RARE, null, Type.MAGE, 4, 3, 2, 1);

        //when
        unitService.levelUp(rareMage);

        //then
        assertEquals(5, rareMage.getLevel());
        assertEquals(1, rareMage.getCurrentExperience());
        assertEquals(135, rareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, rareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, rareMage.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE), rareMage.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareKnightThatShouldNotGetNewSpecialAbilityOrIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.KNIGHT, 1, 3, 2, 2);

        //when
        unitService.levelUp(superRareKnight);

        //then
        assertEquals(2, superRareKnight.getLevel());
        assertEquals(1, superRareKnight.getCurrentExperience());
        assertEquals(18, superRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK), 2), superRareKnight.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareWarriorThatShouldNotGetNewSpecialAbilityOrIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.WARRIOR, 1, 3, 2, 2);

        //when
        unitService.levelUp(superRareWarrior);

        //then
        assertEquals(2, superRareWarrior.getLevel());
        assertEquals(1, superRareWarrior.getCurrentExperience());
        assertEquals(18, superRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM), 2), superRareWarrior.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareArcherThatShouldNotGetNewSpecialAbilityOrIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.ARCHER, 1, 3, 2, 2);

        //when
        unitService.levelUp(superRareArcher);

        //then
        assertEquals(2, superRareArcher.getLevel());
        assertEquals(1, superRareArcher.getCurrentExperience());
        assertEquals(18, superRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), 2), superRareArcher.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareMageThatShouldNotGetNewSpecialAbilityOrIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.MAGE, 1, 3, 2, 2);

        //when
        unitService.levelUp(superRareMage);

        //then
        assertEquals(2, superRareMage.getLevel());
        assertEquals(1, superRareMage.getCurrentExperience());
        assertEquals(18, superRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superRareMage.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK), 2), superRareMage.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareKnightThatShouldIncrementCurrentSpecialAbilityAndShouldNotGetNewSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.KNIGHT, 3, 3, 2, 2);

        //when
        unitService.levelUp(superRareKnight);

        //then
        assertEquals(4, superRareKnight.getLevel());
        assertEquals(1, superRareKnight.getCurrentExperience());
        assertEquals(74, superRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY), superRareKnight.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareWarriorThatShouldIncrementCurrentSpecialAbilityAndShouldNotGetNewSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.WARRIOR, 3, 3, 2, 2);

        //when
        unitService.levelUp(superRareWarrior);

        //then
        assertEquals(4, superRareWarrior.getLevel());
        assertEquals(1, superRareWarrior.getCurrentExperience());
        assertEquals(74, superRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM), FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY), superRareWarrior.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareArcherThatShouldIncrementCurrentSpecialAbilityAndShouldNotGetNewSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.ARCHER, 3, 3, 2, 2);

        //when
        unitService.levelUp(superRareArcher);

        //then
        assertEquals(4, superRareArcher.getLevel());
        assertEquals(1, superRareArcher.getCurrentExperience());
        assertEquals(74, superRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY), superRareArcher.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareMageThatShouldIncrementCurrentSpecialAbilityAndShouldNotGetNewSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.MAGE, 3, 3, 2, 2);

        //when
        unitService.levelUp(superRareMage);

        //then
        assertEquals(4, superRareMage.getLevel());
        assertEquals(1, superRareMage.getCurrentExperience());
        assertEquals(74, superRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superRareMage.getHealth());
        assertEquals(UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY), superRareMage.getSpecialAbilities());
    }

    @Test
    public void testLevelUpSuperRareKnightThatShouldGetNewSpecialAbilityAndNotIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.KNIGHT, 4, 3, 2, 2);

        //when
        unitService.levelUp(superRareKnight);

        //then
        assertEquals(5, superRareKnight.getLevel());
        assertEquals(1, superRareKnight.getCurrentExperience());
        assertEquals(135, superRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getHealth());

        Assertions.assertThat(superRareKnight.getSpecialAbilities())
                .hasSize(2)
                .extracting("value")
                .contains(
                        FIRST_SPECIAL_ABILITY_INITIAL_VALUE,
                        NEXT_SPECIAL_ABILITY_INITIAL_VALUE
                );

        Assertions.assertThat(superRareKnight.getSpecialAbilities())
                .extracting("name", "type")
                .containsOnlyOnce(
                        Tuple.tuple("Block", SpecialAbilityType.BLOCK)
                );
    }

    @Test
    public void testLevelUpSuperRareWarriorThatShouldGetNewSpecialAbilityAndNotIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.WARRIOR, 4, 3, 2, 2);

        //when
        unitService.levelUp(superRareWarrior);

        //then
        assertEquals(5, superRareWarrior.getLevel());
        assertEquals(1, superRareWarrior.getCurrentExperience());
        assertEquals(135, superRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getHealth());

        Assertions.assertThat(superRareWarrior.getSpecialAbilities())
                .hasSize(2)
                .extracting("value")
                .contains(
                        FIRST_SPECIAL_ABILITY_INITIAL_VALUE,
                        NEXT_SPECIAL_ABILITY_INITIAL_VALUE
                );

        Assertions.assertThat(superRareWarrior.getSpecialAbilities())
                .extracting("name", "type")
                .containsOnlyOnce(
                        Tuple.tuple("Vampirism", SpecialAbilityType.VAMPIRISM)
                );
    }

    @Test
    public void testLevelUpSuperRareArcherThatShouldGetNewSpecialAbilityAndNotIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.ARCHER, 4, 3, 2, 2);

        //when
        unitService.levelUp(superRareArcher);

        //then
        assertEquals(5, superRareArcher.getLevel());
        assertEquals(1, superRareArcher.getCurrentExperience());
        assertEquals(135, superRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getHealth());

        Assertions.assertThat(superRareArcher.getSpecialAbilities())
                .hasSize(2)
                .extracting("value")
                .contains(
                        FIRST_SPECIAL_ABILITY_INITIAL_VALUE,
                        NEXT_SPECIAL_ABILITY_INITIAL_VALUE
                );

        Assertions.assertThat(superRareArcher.getSpecialAbilities())
                .extracting("name", "type")
                .containsOnlyOnce(
                        Tuple.tuple("Pierce", SpecialAbilityType.PIERCE)
                );
    }

    @Test
    public void testLevelUpSuperRareMageThatShouldGetNewSpecialAbilityAndNotIncrementCurrentSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.MAGE, 4, 3, 2, 2);

        //when
        unitService.levelUp(superRareMage);

        //then
        assertEquals(5, superRareMage.getLevel());
        assertEquals(1, superRareMage.getCurrentExperience());
        assertEquals(135, superRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superRareMage.getHealth());

        Assertions.assertThat(superRareMage.getSpecialAbilities())
                .hasSize(2)
                .extracting("value")
                .contains(
                        FIRST_SPECIAL_ABILITY_INITIAL_VALUE,
                        NEXT_SPECIAL_ABILITY_INITIAL_VALUE
                );

        Assertions.assertThat(superRareMage.getSpecialAbilities())
                .extracting("name", "type")
                .containsOnlyOnce(
                        Tuple.tuple("Increase attack", SpecialAbilityType.INCREASE_ATTACK)
                );
    }

    @Test
    public void testLevelUpSuperRareKnightThatShouldIncrementBothSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.KNIGHT, 5, 3, 2, 2);

        //when
        unitService.levelUp(superRareKnight);

        //then
        assertEquals(6, superRareKnight.getLevel());
        assertEquals(1, superRareKnight.getCurrentExperience());
        assertEquals(226, superRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superRareKnight.getHealth());

        Assertions.assertThat(superRareKnight.getSpecialAbilities())
                .hasSize(2)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperRareWarriorThatShouldIncrementBothSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.VAMPIRISM, SpecialAbilityType.HEAL), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.WARRIOR, 5, 3, 2, 2);

        //when
        unitService.levelUp(superRareWarrior);

        //then
        assertEquals(6, superRareWarrior.getLevel());
        assertEquals(1, superRareWarrior.getCurrentExperience());
        assertEquals(226, superRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superRareWarrior.getHealth());

        Assertions.assertThat(superRareWarrior.getSpecialAbilities())
                .hasSize(2)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperRareArcherThatShouldIncrementBothSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.PIERCE, SpecialAbilityType.HEAL), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.ARCHER, 5, 3, 2, 2);

        //when
        unitService.levelUp(superRareArcher);

        //then
        assertEquals(6, superRareArcher.getLevel());
        assertEquals(1, superRareArcher.getCurrentExperience());
        assertEquals(226, superRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superRareArcher.getHealth());

        Assertions.assertThat(superRareArcher.getSpecialAbilities())
                .hasSize(2)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.PIERCE, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperRareMageThatShouldIncrementBothSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(List.of(SpecialAbilityType.INCREASE_ATTACK, SpecialAbilityType.HEAL), FIRST_SPECIAL_ABILITY_INITIAL_VALUE);
        UnitDto superRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_RARE, specialAbilities, Type.MAGE, 5, 3, 2, 2);

        //when
        unitService.levelUp(superRareMage);

        //then
        assertEquals(6, superRareMage.getLevel());
        assertEquals(1, superRareMage.getCurrentExperience());
        assertEquals(226, superRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superRareMage.getHealth());

        Assertions.assertThat(superRareMage.getSpecialAbilities())
                .hasSize(2)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.INCREASE_ATTACK, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, FIRST_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareKnightThatShouldIncrementOnlyFirstSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.KNIGHT, 1, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareKnight);

        //then
        assertEquals(2, superSuperRareKnight.getLevel());
        assertEquals(1, superSuperRareKnight.getCurrentExperience());
        assertEquals(18, superSuperRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getHealth());

        Assertions.assertThat(superSuperRareKnight.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareWarriorThatShouldIncrementOnlyFirstSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.WARRIOR, 1, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareWarrior);

        //then
        assertEquals(2, superSuperRareWarrior.getLevel());
        assertEquals(1, superSuperRareWarrior.getCurrentExperience());
        assertEquals(18, superSuperRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getHealth());

        Assertions.assertThat(superSuperRareWarrior.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareArcherThatShouldIncrementOnlyFirstSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.ARCHER, 1, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareArcher);

        //then
        assertEquals(2, superSuperRareArcher.getLevel());
        assertEquals(1, superSuperRareArcher.getCurrentExperience());
        assertEquals(18, superSuperRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getHealth());

        Assertions.assertThat(superSuperRareArcher.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareMageThatShouldIncrementOnlyFirstSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.MAGE, 1, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareMage);

        //then
        assertEquals(2, superSuperRareMage.getLevel());
        assertEquals(1, superSuperRareMage.getCurrentExperience());
        assertEquals(18, superSuperRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getHealth());

        Assertions.assertThat(superSuperRareMage.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareKnightThatShouldIncrementFirstAndSecondSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.KNIGHT, 3, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareKnight);

        //then
        assertEquals(4, superSuperRareKnight.getLevel());
        assertEquals(1, superSuperRareKnight.getCurrentExperience());
        assertEquals(74, superSuperRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getHealth());

        Assertions.assertThat(superSuperRareKnight.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareWarriorThatShouldIncrementFirstAndSecondSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.WARRIOR, 3, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareWarrior);

        //then
        assertEquals(4, superSuperRareWarrior.getLevel());
        assertEquals(1, superSuperRareWarrior.getCurrentExperience());
        assertEquals(74, superSuperRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getHealth());

        Assertions.assertThat(superSuperRareWarrior.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareArcherThatShouldIncrementFirstAndSecondSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.ARCHER, 3, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareArcher);

        //then
        assertEquals(4, superSuperRareArcher.getLevel());
        assertEquals(1, superSuperRareArcher.getCurrentExperience());
        assertEquals(74, superSuperRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getHealth());

        Assertions.assertThat(superSuperRareArcher.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareMageThatShouldIncrementFirstAndSecondSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.MAGE, 3, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareMage);

        //then
        assertEquals(4, superSuperRareMage.getLevel());
        assertEquals(1, superSuperRareMage.getCurrentExperience());
        assertEquals(74, superSuperRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getHealth());

        Assertions.assertThat(superSuperRareMage.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareKnightThatShouldIncrementFirstAndThirdSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.KNIGHT, 8, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareKnight);

        //then
        assertEquals(9, superSuperRareKnight.getLevel());
        assertEquals(1, superSuperRareKnight.getCurrentExperience());
        assertEquals(739, superSuperRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getHealth());

        Assertions.assertThat(superSuperRareKnight.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareWarriorThatShouldIncrementFirstAndThirdSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.WARRIOR, 8, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareWarrior);

        //then
        assertEquals(9, superSuperRareWarrior.getLevel());
        assertEquals(1, superSuperRareWarrior.getCurrentExperience());
        assertEquals(739, superSuperRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getHealth());

        Assertions.assertThat(superSuperRareWarrior.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareArcherThatShouldIncrementFirstAndThirdSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.ARCHER, 8, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareArcher);

        //then
        assertEquals(9, superSuperRareArcher.getLevel());
        assertEquals(1, superSuperRareArcher.getCurrentExperience());
        assertEquals(739, superSuperRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getHealth());

        Assertions.assertThat(superSuperRareArcher.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareMageThatShouldIncrementFirstAndThirdSpecialAbility() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.MAGE, 8, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareMage);

        //then
        assertEquals(9, superSuperRareMage.getLevel());
        assertEquals(1, superSuperRareMage.getCurrentExperience());
        assertEquals(739, superSuperRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getHealth());

        Assertions.assertThat(superSuperRareMage.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareKnightThatShouldIncrementAllSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareKnight =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.KNIGHT, 5, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareKnight);

        //then
        assertEquals(6, superSuperRareKnight.getLevel());
        assertEquals(1, superSuperRareKnight.getCurrentExperience());
        assertEquals(226, superSuperRareKnight.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_KNIGHT, superSuperRareKnight.getHealth());

        Assertions.assertThat(superSuperRareKnight.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareWarriorThatShouldIncrementAllSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareWarrior =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.WARRIOR, 5, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareWarrior);

        //then
        assertEquals(6, superSuperRareWarrior.getLevel());
        assertEquals(1, superSuperRareWarrior.getCurrentExperience());
        assertEquals(226, superSuperRareWarrior.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_WARRIOR, superSuperRareWarrior.getHealth());

        Assertions.assertThat(superSuperRareWarrior.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareArcherThatShouldIncrementAllSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareArcher =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.ARCHER, 5, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareArcher);

        //then
        assertEquals(6, superSuperRareArcher.getLevel());
        assertEquals(1, superSuperRareArcher.getCurrentExperience());
        assertEquals(226, superSuperRareArcher.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_ARCHER, superSuperRareArcher.getHealth());

        Assertions.assertThat(superSuperRareArcher.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }

    @Test
    public void testLevelUpSuperSuperRareMageThatShouldIncrementAllSpecialAbilities() {
        //given
        List<SpecialAbilityDto> specialAbilities = UnitServiceTestUtils.createSpecialAbilities(
                List.of(SpecialAbilityType.BLOCK, SpecialAbilityType.HEAL, SpecialAbilityType.VAMPIRISM),
                SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE
        );
        UnitDto superSuperRareMage =
                UnitServiceTestUtils.createUnit(Rarity.SUPER_SUPER_RARE, specialAbilities, Type.MAGE, 5, 3, 2, 3);

        //when
        unitService.levelUp(superSuperRareMage);

        //then
        assertEquals(6, superSuperRareMage.getLevel());
        assertEquals(1, superSuperRareMage.getCurrentExperience());
        assertEquals(226, superSuperRareMage.getNextLevelExperience());
        assertEquals(UnitServiceTestUtils.DEFAULT_ATTACK + ATTACK_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getAttack());
        assertEquals(UnitServiceTestUtils.DEFAULT_HEALTH + HEALTH_INCREMENT_VALUE_FOR_MAGE, superSuperRareMage.getHealth());

        Assertions.assertThat(superSuperRareMage.getSpecialAbilities())
                .hasSize(3)
                .contains(
                        new SpecialAbilityDto(SpecialAbilityType.BLOCK, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_FIRST_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.HEAL, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY),
                        new SpecialAbilityDto(SpecialAbilityType.VAMPIRISM, SUPER_SUPER_RARE_SPECIAL_ABILITY_INITIAL_VALUE + INCREMENT_VALUE_FOR_NEXT_SPECIAL_ABILITY)
                );
    }
}